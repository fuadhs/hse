/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const satuanRoutes = {
  path: '/satuan',
  component: Layout,
  redirect: '/satuan/list',
  name: 'Master Satuan',
  meta: {
    title: 'Master',
    icon: 'example',
  },
  children: [
    /** Master Satuan */
    {
      path: 'satuan',
      component: () => import('@/views/master/MasterSatuan'),
      name: 'satuan',
      meta: { title: 'Master Barang', icon: 'table' },
    },
    {
      path: 'edit/:id(\\d+)',
      component: () => import('@/views/master/Edit'),
      name: 'EditBarang',
      meta: { title: 'editBarang', noCache: true },
      hidden: true,
    },
    {
      path: 'list',
      component: () => import('@/views/master/List'),
      name: 'barangList',
      meta: { title: 'barangList', icon: 'list' },
    },
  ],
};

export default satuanRoutes;
