/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const BarangRoutes = {
  path: '/barang',
  component: Layout,
  redirect: '/Master/list',
  name: 'Master',
  meta: {
    title: 'Master',
    icon: 'example',
  },
  children: [
    {
      path: 'create',
      component: () => import('@/views/master/Create'),
      name: 'CreateBarang',
      meta: { title: 'createBarang', icon: 'edit' },
    },
    {
      path: 'edit/:id(\\d+)',
      component: () => import('@/views/master/Edit'),
      name: 'EditBarang',
      meta: { title: 'editBarang', noCache: true },
      hidden: true,
    },
    {
      path: 'list',
      component: () => import('@/views/master/List'),
      name: 'barangList',
      meta: { title: 'barangList', icon: 'list' },
    },
  ],
};

export default BarangRoutes;
