/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const masterRoutes = {
  path: '/master',
  component: Layout,
  redirect: '/master/barang',
  name: 'Master',
  meta: {
    title: 'Master',
    icon: 'nested',
    // permissions: ['view menu nested routes'],
  },
  children: [
    {
      path: 'barang',
      component: () => import('@/views/master/barang/barang'), // Parent router-view
      name: 'Barang',
      meta: { title: 'Barang', icon: 'table' },
    },
    {
      path: 'Divisi',
      component: () => import('@/views/master/Divisi'),
      name: 'Divisi',
      meta: { title: 'Divisi', icon: 'table' },
    },
    {
      path: 'satuan',
      component: () => import('@/views/master/Satuan'),
      name: 'Satuan',
      meta: { title: 'Satuan', icon: 'table' },
    },
    {
      path: 'kategori',
      component: () => import('@/views/master/kategori/List'),
      name: 'Kategori',
      meta: { title: 'Kategori', icon: 'table' },
    },

  ],
};

export default masterRoutes;
