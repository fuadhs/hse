import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/kategori',
    method: 'get',
    params: query,
  });
}

export function fetchKategori(id) {
  return request({
    url: '/kategori/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/kategori/' + id + '/pageviews',
    method: 'get',
  });
}

export function createKategori(data) {
  return request({
    url: '/kategori/create',
    method: 'post',
    data,
  });
}

export function updateKategori(data) {
  return request({
    url: '/kategori/update',
    method: 'post',
    data,
  });
}
