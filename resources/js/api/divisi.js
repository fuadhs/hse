import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/divisi',
    method: 'get',
    params: query,
  });
}

export function fetchDivisi(id) {
  return request({
    url: '/divisi/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/divisi/' + id + '/pageviews',
    method: 'get',
  });
}

export function createDivisi(data) {
  return request({
    url: '/divisi/create',
    method: 'post',
    data,
  });
}

export function updateDivisi(data) {
  return request({
    url: '/divisi/update',
    method: 'post',
    data,
  });
}
