import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/barang',
    method: 'get',
    params: query,
  });
}

export function fetchBarang(id) {
  return request({
    url: '/barang/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/barang/' + id + '/pageviews',
    method: 'get',
  });
}

export function createBarang(data) {
  return request({
    url: '/barang/store',
    method: 'put',
    data,
  });
}

export function updateBarang(data) {
  return request({
    url: '/barang/update',
    method: 'post',
    data,
  });
}
