import request from '@/utils/request';

export function userSearch(name) {
  return request({
    url: '/search/user',
    method: 'get',
    params: { name },
  });
}
export function BarangSearch(barang) {
  return request({
    url: '/search/barang',
    method: 'get',
    params: { barang },
  });
}
