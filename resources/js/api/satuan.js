import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/satuan',
    method: 'get',
    params: query,
  });
}

export function fetchSatuan(id) {
  return request({
    url: '/satuan/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/satuan/' + id + '/pageviews',
    method: 'get',
  });
}

export function createSatuan(data) {
  return request({
    url: '/satuan/create',
    method: 'post',
    data,
  });
}

export function updateSatuan(data) {
  return request({
    url: '/satuan/update',
    method: 'post',
    data,
  });
}
