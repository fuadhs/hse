<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderBarangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'tgl' => $this->uuid,
            'order_id' => $this->order['id'],
            'barang' => $this->barang['nama_barang'],
            'qty'=> $this->qty,
            'status'=>$this->status_barang,
            'creator' => $this->user['name'],
        ];
    }
}
