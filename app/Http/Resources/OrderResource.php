<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'nama_pegawai' => $this->nama_pegawai,
            'status' => $this->status_order,
            // 'limit_stok'=> $this->limit_stok,
            'creator' => $this->user['name'],
        ];
    }
}
