<?php

namespace App\Http\Resources;

// use App\Laravue\Models\User;
// use App\Models\KategoriModel;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\KategoriResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BarangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'barang_id' => $this->barang_id,
            'nama_barang' => $this->nama_barang,
            'kategori' => $this->kategori['kategori'],
            'limit_stok'=> $this->limit_stok,
            'creator' => $this->user['name'],
        ];

    }
    public $collects = user::class;
}
