<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IsiComboResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            // 'id' => $this->id,
            // 'barang_id' => $this->barang_id,
            'label' => $this->kategori,
            'value' => $this->id,
            // 'kategori' => $this->kategori['kategori'],
            // 'limit_stok'=> $this->limit_stok,
            // 'creator' => $this->user['name'],
        ];
    }
}
