<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BarangSatuanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
            return [
            'id' => $this->id,
            'barang_id' => $this->barang_id,
            'barcode'=>$this->barang['barang_id'],
            'nama_barang'=>$this->barang['nama_barang'],
            'kode_barang' => $this->kd_barang,
            'satuan' => $this->satuan,
            'konversi' => $this->konversi,
            'harga'=> $this->harga,
            'creator' => $this->user['name'],
        ];
    }
}
