<?php

namespace App\Http\Controllers;

use App\Http\Resources\IsiComboResource;
use App\Models\KategoriModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IsiComboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function cbkategori()
    {
        return response()->json(KategoriModel::select('id','kategori')->get());
    }

}
