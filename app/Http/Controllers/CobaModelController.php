<?php

namespace App\Http\Controllers;

use App\Models\CobaModel;
use Illuminate\Http\Request;

class CobaModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CobaModel  $cobaModel
     * @return \Illuminate\Http\Response
     */
    public function show(CobaModel $cobaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CobaModel  $cobaModel
     * @return \Illuminate\Http\Response
     */
    public function edit(CobaModel $cobaModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CobaModel  $cobaModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CobaModel $cobaModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CobaModel  $cobaModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(CobaModel $cobaModel)
    {
        //
    }
}
