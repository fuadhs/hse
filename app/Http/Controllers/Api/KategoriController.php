<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KategoriModel;
use App\Laravue\JSonResponse;
use App\Http\Resources\KategoriResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    const ITEM_PER_PAGE = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $kategori = KategoriModel::all();
       $cariparams = $request->all();
       $kategoriQuery = KategoriModel::query();
       $limit = Arr::get($cariparams,'limit',static::ITEM_PER_PAGE);
       $keyword = Arr::get($cariparams,'keyword','');
       if (!empty($keyword)){
           $kategoriQuery->where('kategori','LIKE','%'.$keyword.'%');
       }
       return KategoriResource::collection($kategoriQuery->paginate($limit));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            $validator = Validator::make(
                $request->all(),
                [
                    'kategori' => ['required']
                ]
            );
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 403);
            } else {
                $params = $request->all();
                $category = KategoriModel::create([
                    'kategori' => $params['kategori'],
                    'user_id' => $params['user_id'],
                    // 'code' => strtolower($params['name']) . time(), // Just to make sure this value is unique
                    // 'description' => $params['description'],
                ]);
                return new KategoriResource($category);
            }
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
