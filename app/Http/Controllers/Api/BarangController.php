<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BarangCollection;
use Illuminate\Http\Request;
use App\Models\BarangModel;
use App\Laravue\JsonResponse;
// use App\Laravue\Models\Permission;
// use App\Laravue\Models\Role;
// use App\Laravue\Models\User;
// use Illuminate\Http\Request;
use App\Http\Resources\BarangResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class BarangController extends BaseController
{
    const ITEM_PER_PAGE = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    public function index(Request $request)
    {

        // $barangQuery1 =BarangModel::select('m_barang')
        // ->join('m_kategori','m_barang.kategori_id','=','m_kategori.id')
        // ->join('users','m_barang.user_id','=','users.id')
        // ->select('m_barang.*','m_kategori.kategori','users.name')
        // ->get();
        $searchParams = $request->all();
        $barangQuery = BarangModel::query();

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $kategori = Arr::get($searchParams, 'kategori', '');
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($kategori)) {
            $barangQuery->whereHas('id', function($q) use ($kategori) { $q->where('id', $kategori); });
        }

        if (!empty($keyword)) {
            $barangQuery->where('barang_id', 'LIKE', '%' . $keyword . '%');
            $barangQuery->orWhere('nama_barang', 'LIKE', '%' . $keyword . '%');
        }
        return BarangResource::collection($barangQuery->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $validator = Validator::make(
                $request->all(),
                [
                    'barang_id' => ['required']
                ]
            );

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 403);
            } else {
                $params = $request->all();
                $barang = BarangModel::create([
                    'barang_id' => $params['barang_id'],
                    'nama_barang' => $params['nama_barang'],
                    // 'code' => strtolower($params['name']) . time(), // Just to make sure this value is unique
                    'kategori_id' => $params['kategori_id'],
                    'limit_stok' => $params['limit_stok'],
                    'status' => $params['status'],
                    'user_id' => $params['user_id']
                ]);

                return new BarangResource($barang);
            }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
