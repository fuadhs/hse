<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderBarangResource;
use App\Models\OrderBarangModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class OrderBarangController extends Controller
{
    const ITEM_PER_PAGE = 15;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $orderBarangQuery = OrderBarangModel::query();

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $kategori = Arr::get($searchParams, 'kategori', '');
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($kategori)) {
            $orderBarangQuery->whereHas('id', function($q) use ($kategori) { $q->where('id', $kategori); });
        }

        if (!empty($keyword)) {
            $orderBarangQuery->where('barang_id', 'LIKE', '%' . $keyword . '%');
            $orderBarangQuery->orWhere('nama_barang', 'LIKE', '%' . $keyword . '%');
        }
        return OrderBarangResource::collection($orderBarangQuery->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
