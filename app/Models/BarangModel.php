<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = 'm_barang';
    // public $timestamps = true;
    protected $fillable = array('barang_id', 'nama_barang', 'kategori_id', 'limit_stok', 'user_id', 'periode_id');
    protected $hidden = array('id','created_at','updated_at');
    protected $appends = ['BarangTotal'];
//    Barang kategori
    public function kategori()
    {
        return $this->belongsTo(KategoriModel::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function order_barang()
    {
        return $this->hasMany(OrderBarangModel::class);
    }

}
