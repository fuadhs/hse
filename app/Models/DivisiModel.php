<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class DivisiModel extends Model
{
    protected $table = 'm_divisi';
    protected $fillable = array('kd_divisi','nama_divisi');

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
