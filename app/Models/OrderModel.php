<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = 't_order';
    protected $fillable = ['tgl','divisi_id','nama_pegawai','order_status','user_id'];

    public function orderBarang()
    {
        return $this->hasMany(OrderBarangModel::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
