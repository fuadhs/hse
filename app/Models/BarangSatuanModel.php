<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class BarangSatuanModel extends Model
{
    protected $table ='m_barang_satuan';
    protected $fillable=array('kd_barang','satuan','konversi','harga');
    public function barang()
    {
        return $this->belongsTo(BarangModel::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
