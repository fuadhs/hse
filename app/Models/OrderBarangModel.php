<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class OrderBarangModel extends Model
{
    protected $table = 't_order_barang';
    protected $fillable = ['tgl','order_id','barang_id','qty','status_barang','user_id'];

    public function order()
    {
       return $this->belongsTo(OrderModel::class);
    }
    // public function order_barang()
    // {
    //     return $this->hasMany(OrderModel::class);
    // }
    public function barang()
    {
        return $this->belongsTo(BarangModel::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
