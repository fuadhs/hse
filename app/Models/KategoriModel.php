<?php

namespace App\Models;

use App\Http\Resources\usersResource;
use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = 'm_kategori';
    // public $timestamps = true;
    protected $fillable = array('kategori','user_id');
    public function barang()
    {
       return $this->hasOne(BarangModel::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
