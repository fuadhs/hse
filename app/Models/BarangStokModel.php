<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangStokModel extends Model
{
    protected $table = 'vw_stok_barang';
    // protected $fillable = array('awal','akhir','keluar');
    public function barang()
    {
        return $this->belongsTo('App\Models\ModelBarang','barang_id');
    }
}
