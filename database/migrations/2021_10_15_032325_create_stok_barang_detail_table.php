<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokBarangDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_stok_barang_detail', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('stok_id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->float('qty');
            $table->tinyInteger('status_stok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_stok_barang_detail');
    }
}
