<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_order_barang', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('kd_order');
            $table->dateTime('tgl');
            $table->integer('order_id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->float('qty');
            $table->tinyInteger('status_barang');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_order_barang');
    }
}
