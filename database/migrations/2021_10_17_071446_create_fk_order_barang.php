<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFkOrderBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('t_order')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->foreign('barang_id')->references('id')->on('m_barang')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->dropForeign('t_order_barang_user_id_foreign');
		});
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->dropForeign('t_order_barang_order_id_foreign');
		});
        Schema::table('t_order_barang', function(Blueprint $table) {
			$table->dropForeign('t_order_barang_barang_id_foreign');
		});
    }
}
