<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_barang_stok', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->decimal('awal');
            $table->decimal('masuk');
            $table->decimal('keluar');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_barang_stok');
    }
}
