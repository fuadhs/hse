<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangSatuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_barang_satuan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('barang_id')->unsigned();
            $table->string('kd_barang');
            $table->string('satuan');
            $table->integer('konversi');
            $table->double('harga');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            // $table->foreign('kd_barang')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_barang_satuan');
    }
}
