<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFkBarangKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('m_barang', function(Blueprint $table) {
			$table->foreign('kategori_id')->references('id')->on('m_kategori')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('m_barang', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		// Schema::table('transaction', function(Blueprint $table) {
		// 	$table->foreign('warehouse_id')->references('id')->on('warehouse')
		// 				->onDelete('set null')
		// 				->onUpdate('set null');
		// });
		// Schema::table('transaction', function(Blueprint $table) {
		// 	$table->foreign('periode_id')->references('id')->on('periode')
		// 				->onDelete('cascade')
		// 				->onUpdate('cascade');
		// });
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_barang', function(Blueprint $table) {
			$table->dropForeign('m_barang_kategori_id_foreign');
		});
        Schema::table('m_barang', function(Blueprint $table) {
			$table->dropForeign('m_barang_user_id_foreign');
		});
    }
}
