<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFkBarangSatuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('m_barang_satuan', function(Blueprint $table) {
			$table->foreign('barang_id')->references('id')->on('m_barang')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('m_barang_satuan', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_barang_satuan', function(Blueprint $table) {
			$table->dropForeign('m_barang_satuan_barang_id_foreign');
		});
        Schema::table('m_barang_satuan', function(Blueprint $table) {
			$table->dropForeign('m_barang_satuan_user_id_foreign');
		});
    }
}
